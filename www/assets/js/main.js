// Import third-party libraries with Webpack
var jQuery = require('jquery');
// Comment previous line and uncomment the next if you want to use jQuery globally
// var jQuery = require("expose-loader?jQuery!jquery");

(function($) {

	/*
	$.post(wp_ajax.ajaxurl, { action: 'ACTION' }, function(data) {
		// results come back in JSON, format data.property
	}, 'json');
	*/

	$(document).ready(function() {

  ns.slider.list.push("#ns-section-testimonial");
  ns.slider.initialize();


  (function() {
    $(".ns-fancy-intro-phrase-letter").each(
      function() {
        $("<style type='text/css'>span.ns-fancy-intro-phrase-letter-" + $(this).text() + "::after { content: '" + $(this).text() + "' !important; }</style>").appendTo("head");
        $(this).addClass("ns-fancy-intro-phrase-letter-" + $(this).text());
      }
    );
  })();

  $('#ns-article-content').columnize({
    columns: 3,
    lastNeverTallest: true
  });
  ns.article.formatForViewport();
  $('#ns-article-share-link').val(window.location.href);

  $(function() {
    var wall = new Freewall("#ns-articles-list");
    wall.reset({
      selector: '.ns-articles-list-item',
      animate: true,
      cellW: '400',
      cellH: 'auto',
      gutterX: 48,
      gutterY: 63,
      onResize: function() {
        wall.fitWidth();
      }
    });
    wall.fitWidth();

    // wall.container.find('.ns-articles-list-item img').load(function() {
    //   wall.fitWidth();
    // });
    // wall.fitWidth();

  });

  $(window).scroll(function() {

    if ( $(window).scrollTop() > 0 && !$('body').hasClass("ns-menu-responsive-visible") ) {
      ns.hamburger.makeIconSmall();
    }
    else if ( $(window).scrollTop() <= 0 ) {
      ns.hamburger.makeIconLarge();
    }

  });

  $(window).resize(function() {

    // $('#ns-article-content').columnize({
    //   columns: 3,
    //   lastNeverTallest: true
    // });
    // $('#ns-article-content').uncolumnize();

    ns.article.formatForViewport();

  });

});

var ns = {
  scroll: {
    disable: function() {
      var disableScroll = function() {
        $('body').addClass("ns-scroll-disable");
      };
      setTimeout(disableScroll, 500);
    },
    enable: function() {
      $('body').removeClass("ns-scroll-disable");
      var enableScroll = function() {
        $('body').removeClass("ns-scroll-disable");
      };
      setTimeout(enableScroll, 250);
    }
  },
  biography: {
    current: false,
    set: function(personToShow) {
      ns.biography.current = personToShow;
      $("#ns-biography-modal-" + ns.biography.current).addClass("ns-biography-modal-visible");
      // disableScroll();
      ns.scroll.disable();
    },
    hide: function(teamMember) {
      $("#ns-biography-modal-" + ns.biography.current).removeClass("ns-biography-modal-visible");
      ns.biography.current = false;
      // enableScroll();
      ns.scroll.enable();
      if ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        document.getElementById('ns-member-' + teamMember).scrollIntoView();
      }
      else {
        document.getElementById('ns-team-section').scrollIntoView();
      }
    }
  },
  slider: {
    list: [],
    initialize: function() {
      for (var i = 0; i < ns.slider.list.length; i++) {
        $(ns.slider.list[i] + "> li").first().addClass("ns-slider-current");
      }
    },
    switch: function(currentSlider, direction) {
      var currentSlideNumber = 0;
      var setCurrentSlideNumber = function(slideNumber) {
        if ( $(currentSlider + "> li").eq(slideNumber).hasClass("ns-slider-current") ) {
          currentSlideNumber = slideNumber;
        }
      };
      var removeCurrentSlideClass = function(slideNumber) {
        $(currentSlider + "> li").eq(slideNumber).removeClass("ns-slider-current");
      };
      $(currentSlider + "> li").each(setCurrentSlideNumber);
      $(currentSlider + "> li").each(removeCurrentSlideClass);
      if ( direction === 'forward' ) {
        if ( currentSlideNumber + 1 < $(currentSlider + "> li").length ) {
          currentSlideNumber++;
        }
        else { currentSlideNumber = 0; }
      }
      else {
        if ( currentSlideNumber === 0 ) {
          currentSlideNumber = $(currentSlider + "> li").length - 1;
        }
        else { currentSlideNumber--; }
      }
      $(currentSlider + "> li").eq(currentSlideNumber).addClass("ns-slider-current");
    }
  },
  hamburger: {
// ns-menu-responsive-visible
    show: function() {
      $('body').addClass("ns-menu-responsive-visible");
      ns.scroll.disable();
    },
    hide: function() {
      $('body').removeClass("ns-menu-responsive-visible");
      ns.scroll.enable();
      if ( $(window).scrollTop() > 0 ) {
        ns.hamburger.makeIconSmall();
      }
    },
    toggle: function() {
      if ( $('body').hasClass("ns-menu-responsive-visible") ) {
        ns.hamburger.hide();
        if ( $(window).scrollTop() > 0 ) {
          ns.hamburger.makeIconSmall();
        }
      }
      else {
        ns.hamburger.show();
        ns.hamburger.makeIconLarge();
      }

    },
    makeIconSmall: function() {
      $('#ns-menu-responsive-icon').addClass("ns-menu-responsive-icon-small");
    },
    makeIconLarge: function() {
      $('#ns-menu-responsive-icon').removeClass("ns-menu-responsive-icon-small");
    }
  },
  article: {
    formatForViewport: function() {
      if ( $(document).width() < 768 ) {
        if (!$('#ns-article').hasClass("ns-article-single-column")) {
          $('#ns-article-content').uncolumnize();
          $('#ns-article').addClass("ns-article-single-column");
        }
      }
    },
    toggleView: function(state) {
      if (state === true) {
        if ($('#ns-article').hasClass("ns-article-single-column")) {
          $('#ns-article-content').columnize({
            columns: 3,
            lastNeverTallest: true
          });
          $('#ns-article').removeClass("ns-article-single-column");
        }
      }
      else if (state === false) {
        if (!$('#ns-article').hasClass("ns-article-single-column")) {
          $('#ns-article-content').uncolumnize();
          $('#ns-article').addClass("ns-article-single-column");
        }
      }
      else {
        if ($('#ns-article').hasClass("ns-article-single-column")) {
          $('#ns-article-content').columnize({
            columns: 3,
            lastNeverTallest: true
          });
          $('#ns-article').removeClass("ns-article-single-column");
        }
        else {
          $('#ns-article-content').uncolumnize();
          $('#ns-article').addClass("ns-article-single-column");
        }
      }
    }
  }
};














}(jQuery));
