<?php
require_once(get_stylesheet_directory() . '/inc/admin.php');
require_once(get_stylesheet_directory() . '/inc/ajax.php');
require_once(get_stylesheet_directory() . '/inc/base.php');
require_once(get_stylesheet_directory() . '/inc/config.php');
require_once(get_stylesheet_directory() . '/inc/front.php');
require_once(get_stylesheet_directory() . '/inc/models.php');
require_once(get_stylesheet_directory() . '/inc/project.php');
require_once(get_stylesheet_directory() . '/inc/singleton.php');
