// Import third-party libraries with Webpack
var jQuery = require('jquery');
var swiper = require('swiper');
var anime  = require('animejs');
var TweenMax = require('gsap/TweenMax');
// Comment previous line and uncomment the next if you want to use jQuery globally
// var jQuery = require("expose-loader?jQuery!jquery");

(function($) {

	$(document).ready(function() {
		/*
		$.post(wp_ajax.ajaxurl, { action: 'ACTION' }, function(data) {
			// results come back in JSON, format data.property
		}, 'json');
		*/

		// Swiper initialize
		var swiper = new Swiper ('.swiper-container');

		// Nav menu
		var menuButton = '.hamburger';
		var mobileMenu = '.mobile-modal';

		$(menuButton).on( 'click', function(){
			$(menuButton).toggleClass('is-active');
			$(mobileMenu).toggleClass('open');
		});

		// Greensocks Hexagon Animations
		// var $hexagon = document.querySelectorAll('#team-hexagon');
		//
		// var tween = TweenLite.to($box, 2, {
		//
		// })

		// Hexagon Biography Animations
		// var hexagonTimeline = anime.timeline({
		// 	autoplay: false
		// });
		//
		// var hexagonGrid = document.querySelectorAll('#team-hexagon');
		// var clickedHexagon = this.hexagonGrid;
		// var hexagon = $('.hexes');
		// var modal = $('.bio-modal');
		// var singularHexagon = $(this).hexagon;
		// var hexagonHexes = hexagonGrid + " " + hexagon;
		// var clickedHexagon;
		// var hexModal = clickedHexagon + ".bio-modal";
		// var hexHexagon = clickedHexagon + ".hexes";
		// console.log(hexagonGrid);
		// hexagonTimeline.add({
		// 		targets: function(){
		// 			return $(this).find(modal);
		// 		},
		// 		duration: 1000,
		// 		opacity: 1,
		// 		easing: 'easeInCubic',
		// 		direction: 'alternate'
		// 	})
		// 	.add({
		// 		targets: function() {
		// 			return $(this).find(hexagon);
		// 		},
		// 		duration: 300,
		// 		opacity: 0,
		// 		offset: 0,
		// 		easing: 'easeInCubic'
		// 	});
		//
		//
		// $(hexagonGrid).on('click', function() {
		// 	hexagonTimeline.play();
		// 	hexagonTimeline.reverse();
		// 	console.log($(this).find(hexagon));
		// });

	});

}(jQuery));
